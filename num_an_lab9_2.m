f=@(x) cos(77*asin(0.3*sqrt((x+4)/7.7)));

lp = -2; 
rp = 2;

h = 0.01*(rp-lp);

x0 = [-1, 0, 2];
eps = 2.^-(4:1:20);

eps_option = 1;
for x_option =1:3
    i=0;
    err=eps+1;
    x=x0(x_option);
    dx0=my_df(f,x,h);
    for eps_option=1:length(eps)
        while err>eps(eps_option)
            x = x-f(x)/dx0;
            err=abs(f(x));
            disp(err);
            i=i+1;
            if i>10000 break; end
        end
        %{
        disp('x_found')
        disp(x)
        disp(f(x))
        %}
        figure(x_option);
        scatter(eps(eps_option),i,20,'o','filled',"black");
        hold on;
    end
    title(['root № ',num2str(x_option),' \approx ',num2str(x)])
    xlabel('\epsilon')
    ylabel('iterations');
    fontsize(gca,20,"points")
    xlim([0 eps(1)*1.1]);
    ylim([0 inf]);
    grid;
    hold off
end

function df = my_df(f,x,h)
    df = (f(x+h/2)-f(x-h/2))/h;
end
